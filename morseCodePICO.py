from machine import Pin
import time

"""
Morse Code 
29-Jan-2021
gitlab.com/fiorecosta"
"""

led = Pin(25, Pin.OUT)

# Set base time for dot.
baseTime = 0.09
# Set spacing time between letters (units)
sT = 3 * baseTime

# Set spacing between words (units)
wT = 7 * baseTime

#Dot Time
dT= baseTime
#Dash Time
dS = dT * 3

# dot function illuminating LED for a '.' with desingated timings
def dot():
    print(".", end='')
    led.high()
    time.sleep(dT)
    led.low()
    time.sleep(sT)
    
# dot function illuminating LED for a '-' with designated timings
def dash():
    print("-", end='')
    led.high()
    time.sleep(dS)
    led.low()
    time.sleep(sT)
    
# gap between worlds and insert / for text
def wGap():
    print("/",end=" ")
    time.sleep(wT)
    
# Morse Code alphabet 
morseDict = {
'A':'.- ',
'B':'-... ',
'C':'-.-. ',
'D':'-.. ',
'E':'. ',
'F':'..-. ',
'G':'--. ',
'H':'.... ',
'I':'.. ',
'J':'.--- ',
'K':'-.- ',
'L':'.-.. ',
'M':'-- ',
'N':'-. ',
'O':'--- ',
'P':'.--. ',
'Q':'--.- ',
'R':'.-. ',
'S':'... ',
'T':'- ',
'U':'..- ',
'V':'...- ',
'W':'.-- ',
'X':'-..- ',
'Y':'-.-- ',
'Z':'--.. ',
'1':'.---- ',
'2':'..--- ',
'3':'...-- ',
'4':'....- ',
'5':'..... ',
'6':'-.... ',
'7':'--... ',
'8':'---.. ',
'9':'----. ',
'0':'----- ',
'':'--..-- ',
'.':'.-.-.- ',
'?':'..--.. ',
'!':'.----. ',
' ':' ',
'/':'/ ',
'a':'.- ',
'b':'-... ',
'c':'-.-. ',
'd':'-.. ',
'e':'. ',
'f':'..-. ',
'g':'--. ',
'h':'.... ',
'i':'.. ',
'j':'.--- ',
'k':'-.- ',
'l':'.-.. ',
'm':'-- ',
'n':'-. ',
'o':'--- ',
'p':'.--. ',
'q':'--.- ',
'r':'.-. ',
's':'... ',
't':'- ',
'u':'..- ',
'v':'...- ',
'w':'.-- ',
'x':'-..- ',
'y':'-.-- ',
'z':'--.. '
}

while True: 
    valueM = input("\n Enter a value to Send via Morse >> ")
    valueM = list(valueM)
    
    # convert spaces to '/'
    valueM = ['/' if i==' ' else i for i in valueM]
    
    # Iterate list and convert to morse using the morse code dictionary (morseDict)
    for i in valueM:
        try:
            for i in morseDict[i]:
                if i == "-":
                    dash()
                elif i == ".":
                    dot()
                elif i == " ":
                    print("", end=" ")
                elif i == "/":
                    wGap()
                else:
                    print("<<WARNING : UNDEFINED VALUE>>")
                    False
        except:
            print("<<WARNING : UNDEFINED VALUE>>")
