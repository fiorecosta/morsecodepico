# morseCodePICO
This small application request input from the user , via console and will automatically convert the input to Morse. It will subsequently output the converted morse code to the console whilst blinking the corresponding morse code utilising the on board LED (pin 25).

## About Morse Code
Mores code is a system of converting text and numbers into a binary format of dots and dashes  (dits and dahs) which can be easily sent via electronic signals, lights or sounds. Further reading can be found via [Wikipedia](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjX4KXsjsDuAhUhzTgGHXrTCkYQFjABegQICxAC&url=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FMorse_code&usg=AOvVaw1z5remqLX30epgSQ2UONNM).  


## About the Raspberry Pi Pico
The Pico, made by the Raspberry Pi Foundation is a small board utilising a RP2040 chip. The chip features a dule-core arm Cortex M0+ processor, with 265KB internal RAM. The board features a range of flexible I/O options ranging from SPI, I2C as well as programmable I/O.  In keeping with the foundations core principals of providing educational and maker technology’s at low price points the Pico at lunch cost $4 USD. More information can be found via the [foundations website](https://www.raspberrypi.org/products/raspberry-pi-pico/).

 


